"use strict";
//Task 2
class Person2 {
  constructor(name, age) {
      this.name = name;
      this.age = age;
  }
  describe() {
      console.log(`${this.name} is ${this.age} years old`);
  }
}

const jack = new Person2('Jack', 25);
const jill = new Person2('Jill', 24);

jack.describe();
jill.describe();