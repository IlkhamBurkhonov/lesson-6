"use strict";
class Person {
  constructor(name, age) {
      this.name = name;
      this.age = age;
  }
}
class Baby extends Person {
  constructor(name, age, favToy) {
  super(name, age);
  this.toy = favToy;
  }
  play() {
      console.log(`${this.name} is ${this.age} years old, and playing with ${this.toy}`);
  }
}

const Ilkham = new Baby('Bob', 2, 'Rocket Toy');
const Abror = new Baby('Jeck', 5, 'Pilice car');
Ilkham.play();
Abror.play();




